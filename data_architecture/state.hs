-- An example of using the state monad

import Control.Monad.State
import Data.List

data Room = Entrance Book | SpookyCorridor Book | TreasureRoom Book | ThroneRoom Book | Dungeon Book
    deriving (Show, Eq)
data Door = Locked Phrase Room | Open Room
    deriving Show
data Phrase = OpenSesame | Alakazam | Abracadabra | Emases | Mazakala
    deriving (Show, Eq)
data Trap = Explosion | Poison | Spikes | Magic
    deriving Show

type Book = [Phrase]

getBook (Entrance b) = b
getBook (SpookyCorridor b) = b
getBook (TreasureRoom b) = b
getBook (ThroneRoom b) = b
getBook (Dungeon b) = b

phraseBook = [
    Abracadabra,
    Mazakala,
    OpenSesame
    ]

unlockDoor :: Door -> Phrase -> Either Door Room
unlockDoor (Locked p r) p'
    | p == p' = Right r
    | otherwise = Left (Locked p r)
unlockDoor (Open r) _ = Right r

usePhrase :: Phrase -> Either Door Room -> State Book (Either Door Room)
usePhrase phrase (Left door) = do
    book <- get
    if elem phrase book then do
        put (delete phrase book)
        return $ unlockDoor door phrase
    else
        return (Left door)
usePhrase _ r = return r

findBook :: (Either Door Room) -> State Book (Either Door Room)
findBook (Right room) = modify ((++) (getBook room)) >> return (Right room)
findBook door = return door

encounterDoor :: Door -> State Book (Either Door Room)
encounterDoor door = return (Left door)

-- (Left (Open (SpookyCorridor [])),[Emases,Mazakala])
one = runState (encounterDoor (Locked OpenSesame (Entrance [Emases])) >>= usePhrase Abracadabra >>= usePhrase OpenSesame >>= findBook >> encounterDoor (Open (SpookyCorridor []))) phraseBook
