-- An example of using the Maybe monad

data Room = Entrance | SpookyCorridor | TreasureRoom | ThroneRoom | Dungeon
    deriving Show
data Door = Door Room | TrappedDoor
    deriving Show
data Phrase = OpenSesame | Alakazam | Abracadabra
    deriving Show

magicPhrase :: Phrase -> Room -> Maybe Door
magicPhrase _           Entrance       = Just (Door SpookyCorridor)
magicPhrase Alakazam    SpookyCorridor = Just (TrappedDoor)
magicPhrase _           SpookyCorridor = Nothing
magicPhrase _           TreasureRoom   = Just (TrappedDoor)
magicPhrase OpenSesame  ThroneRoom     = Just (Door Dungeon)
magicPhrase _           ThroneRoom     = Just (TrappedDoor)
magicPhrase Abracadabra Dungeon        = Just (Door Entrance)
magicPhrase _           Dungeon        = Nothing

openDoor :: Door -> Maybe Room
openDoor (Door r) = Just r
openDoor TrappedDoor = Nothing

one = magicPhrase Alakazam Entrance >>= openDoor >>= magicPhrase Alakazam >>= openDoor >>= magicPhrase OpenSesame >>= openDoor
two = magicPhrase OpenSesame Entrance >>= openDoor >>= magicPhrase OpenSesame >>= openDoor
three = magicPhrase OpenSesame ThroneRoom >>= openDoor >>= magicPhrase Abracadabra >>= openDoor
