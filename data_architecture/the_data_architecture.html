<!DOCTYPE html>
<html>
  <head>
    <title>The Data Architecture</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <style type="text/css">
      /* Slideshow styles */
      div.remark-slide-content {
          background: black;
          color: white;
          font-family: sans-serif;
          font-size: x-large;
      }
      div.remark-slide-content.code {
          font-size: initial;
      }
      div.remark-slide-content code {
      }
      div.remark-slide-content .caption {
          font-size: small;
          color: grey;
      }
      div.remark-slide-content h1 {
          font-size: 2em;
          font-variant: small-caps;
      }
      div.remark-slide-content#slide-threeQ1 ul {
          font-size: x-large;
      }
      .right-align {
          float: right;
      }
      .left-align {
          float: left;
      }
      .hrule {
          clear: both;
          display: block;
          border-bottom: solid 1px grey;
          margin: 5px;
      }
      .expand {
          position: relative;
          width: 100%;
          height: 100%;
      }
      .expand code{
          position: absolute;
          top: 0px;
          bottom: 0px;
          left: 0px;
          right: 0px;
      }
      .grey {
          color: grey;
      }
      .smaller ul {
          font-size: smaller;
      }
    </style>
  </head>
  <body>
    <textarea id="source">

class: center, middle

# The Data Architecture

???

I'll pause for questions after each slide.

---
class: center, middle

# The Idea

???

Data is the most important part of the program. Without data we wouldn't have anything to operate on.
So why don't we bring data to the forefront of the design process?

---

# Programs Are Data Pipelines

* Data in, Data out
* Three guiding questions

 1) What do I want

 2) What do I have

 2) How can I get from 1 to 2

???

* Read slide
* I'll go through some examples of how to apply these in the next few slides

Questions?
---
class: center, middle
# What do I want? What do I have?
## Examples

???

Next let's look at some examples of how we'd approach the first two questions.

---
# Easiest

* Situation: I want to write an application that takes form input and turns it into a database entry.
* What do I want?
* What do I have?

???

You can decompose this in several ways. The two obvious ones being:
* I want a final result in a database, and I have an HTTP Request string
* I want a final result in a database, and I have a set of parameters corresponding to form inputs.

---

# Harder

* Situation: I want to make a request to an API and turn its response into data I can store and use later.
* What do I want?
* What do I have?

???
This is a little harder because you have no control over what you have. It's determined by whatever the API gives you.
Let's assume you can't use the data from the API as is, you need to do a little massaging first.
---

# Hardest

* Situation: I'm in an unfamiliar codebase. I need to call a function that expects data in a certain format, but I'm given data in a completely different format.
* What do I want?
* What do I have?

???

This is difficult because you don't have any control over what you have or what you want. You need to figure out how to get from one to the other, preferably as cleanly and as efficiently as you can.

---
class: center, middle

# How do I get there?

---

# Three major tools

* Function composition
* Monads (data composition)
* Arrows (control flow)

---

# Composition

* Remember algebra?
  * `f(x)` and `g(x)`, `g * f = f(g(x))`
* Unix utilities
  * `cat foo | grep "bar" | wc -l`
* Program structure is a single (for now) composition chain made of modular, self-contained parts
* Functions in a composition chain:
  * Tend to be small
  * Tend to perform a single task
  * Are self-contained (operate only on inputs, also called "pure")

???

Composition is simply the piping of some program's output into another program's
input. I say program but that can mean function, method, anything that takes
input and returns output.

In the Haskell code it was more apparent that our programs were simply
continuous chains of composed functions.

Some properties of functions that are meant to be composed is that: <read>

Questions?

Next I'll be showing how composition looks in Ruby and Haskell.
---
class: code
# A Function for Composing Functions

* In Haskell the composition function `(.)` has the type `(.) :: (b -> c) -> (a -> b) -> a -> c`
* In Ruby we'll have to make one

.caption[Ruby]
```Ruby
class Proc
  def self.compose(f, g)
    lambda { |*args| f[g[*args]]
  end
  def *(g)
    Proc.compose(self, g)
  end
end

fizz = lambda { |x| x.to_s << "fizz" }
buzz = lambda { |x| x * 4 }
fizzbuzz = fizz * buzz
fizzbuzz[3] # "12fizz"
```
.hrule[]
.caption[Haskell]
```Haskell
fizz :: String -> String
fizz s = s ++ "fizz"

buzz :: Int -> String
buzz = show . (*4)

fizzbuzz = fizz . buzz $ 3 -- "12fizz"
```

???

Haskell was designed from the ground up to support composition as the main
program structure, so it's no surprise that it's supported without any input on
our part. However Ruby requires some monkey-patching of the Proc class before we
can get something resembling the Haskell code.

We can restructure the Ruby in the previous slides (which, to me, looked
awkward) to follow this new pattern, but I'll leave that as an exercise for the
audience.

Questions?
---
class: smaller

# Monads

* Monads let you compose data types!
* A monad is a type `m` that can be used with the following functions:
  * `bind :: m a -> (a -> m b) -> m b`
  * `return :: a -> m a`
  * `>>=` is the infix operator for `bind`
    * `>> :: m a -> m b -> m b`
  * `m` is the monad's type variable
  * `a` and `b` are type variables as well
    * They are the type the monad contains
    * `a` and `b` can be different types
* `bind` and `return` have laws they need to follow
  * Left Identity: `return a >>= f = f a`
  * Right Identity: `m >>= return = m`
  * Associativity
* Some things to note
  * `bind`'s type signature is remarkably similar to composition!
  * Monads are __not__ a Haskell specific concept. They can be implemented in any language!
  * Haskell has some syntactic sugar (do notation) to make working with monads more convenient and pretty

???

A monad is simply something for which the functions bind and return are defined,
and also that bind and return follow a few laws about their behavior. These laws
are the same that govern function composition!

There exist libraries to implement some monads in many languages. I included a
short description of the `Maybe` monad here as it's one of the most popular.

Questions?

Let's examine some previous code in this new context
---

# Monads (examples)

* `Maybe` monad
  * `data Maybe a = Just a | Nothing`
  * Null checking monad
  * Binding `Maybe`'s together will short-circuit the chain if any function returns `Nothing`
* `Either` monad
  * `data Either a b = Left a | Right b`
  * Same as `Maybe` monad except the `null`s can carry information
  * Useful for computations that can raise an error
* `State` monad
  * `data State s a = State { runState :: s -> (a, s) }`
  * Simulates mutable state
  * State is threaded transparently through each bind operation.
* `IO` monad
  * `main` lives here
  * Operates very similarly to the `State` monad

---

# Monads (examples)

.caption[Maybe]
```haskell
magicPhrase :: Phrase -> Room -> Maybe Door
openDoor :: Door -> Maybe Room

-- returns Nothing
one = magicPhrase Alakazam Entrance >>= openDoor >>= 
    magicPhrase Alakazam >>= openDoor 
    >>= magicPhrase OpenSesame >>= openDoor
-- returns Nothing
two = magicPhrase OpenSesame Entrance >>= openDoor 
    >>= magicPhrase OpenSesame >>= openDoor
-- returns Just Entrance
three = magicPhrase OpenSesame ThroneRoom >>= 
    openDoor >>= magicPhrase Abracadabra 
    >>= openDoor
```

---

# Monads (examples)

.caption[Either]
```haskell
magicPhrase :: Phrase -> Room -> Either (Room, Trap) Door
openDoor :: Door -> Either (Room, Trap) Room
disarmExplosion :: Door -> Either (Room, Trap) Door
disarmPoison :: Door -> Either (Room, Trap) Door
disarmSpikes :: Door -> Either (Room, Trap) Door

-- returns Left (ThroneRoom, Poison)
one = magicPhrase Alakazam Entrance >>= openDoor >>= 
    magicPhrase Alakazam >>= openDoor >>= disarmExplosion
    >>= magicPhrase OpenSesame >>= openDoor
-- returns Left (SpookyCorridor, Magic)
two = magicPhrase OpenSesame Entrance >>= openDoor 
    >>= magicPhrase OpenSesame >>= openDoor
-- returns Right Entrance
three = magicPhrase OpenSesame ThroneRoom >>= 
    openDoor >>= magicPhrase Abracadabra 
    >>= openDoor
```

---

# Monads (examples)

.caption[State]
```haskell
unlockDoor :: Door -> Phrase -> Either Door Room
usePhrase 
  :: Phrase -> Either Door Room
        -> State Book (Either Door Room)
findBook 
  :: (Either Door Room) -> State Book (Either Door Room)
encounterDoor :: Door -> State Book (Either Door Room)

-- returns
-- (Left (Open (SpookyCorridor [])),[Emases,Mazakala])
runState
  (encounterDoor (Locked OpenSesame (Entrance [Emases]))
  >>= usePhrase Abracadabra >>= usePhrase OpenSesame >>=
  findBook >> encounterDoor (Open (SpookyCorridor [])))
  phraseBook
```
---

# Arrows

* An arrow is a type that takes and input an returns an output
* A function is an instance of an arrow, but there are many more
  * All monads can be made into arrows
  * For now it's only relevant that functions are arrows
* There are several combinators for arrows that perform the task of embedding control flow
  * Because functions are arrows, we can now embed control flow in our composition chains!

---

# Some Arrow Functions

* General
  * `first :: a b c -> a (b, d) (c, d)`
  * `second :: a b c -> a (d, b) (d, c)`
  * `(***) :: a b c -> a b' c' -> a (b, b') (c, c')`
  * `(&&&) :: a b c -> a b c' -> a b (c, c')`
  * `(<<<) :: a c d -> a b c -> a b d` and it's reverse `(>>>)`
* Conditionals
  * `left :: a b c -> a (Either b d) (Either c d)`
  * `right :: a b c -> a (Either d b) (Either d c)`
  * `(+++) :: a b c -> a b' c' -> a (Either b b') (Either c c')`
  * `(|||) :: a b d -> a c d -> a (Either b c) d`
---

# Arrows in Practice

* Suppose I have some data from a database and I want to simultaneously send it to a browser and save it as a CSV file
  * `data Person = Person String`
  * `toCSV :: [Person] -> String`
  * `toJSON :: [Person] -> String`
  * `convertBoth people = toCSV &&& toJSON $ people`
* Now suppose we want to return data depending on a predicate
  * `useJSON :: People -> Either People People`
  * `convert people = toJSON ||| toCSV $ (useJSON people)`
* Now we have control flow in our application!

---
class: center, middle

# Questions

    </textarea>
    <script src="http://gnab.github.io/remark/downloads/remark-0.6.0.min.js" type="text/javascript">
    </script>
    <script type="text/javascript">
      var slideshow = remark.create();
    </script>
  </body>
</html>
