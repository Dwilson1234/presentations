-- An example of using the Either monad

data Room = Entrance | SpookyCorridor | TreasureRoom | ThroneRoom | Dungeon
    deriving Show
data Door = Door Room | TrappedDoor Trap Room
    deriving Show
data Phrase = OpenSesame | Alakazam | Abracadabra
    deriving Show
data Trap = Explosion | Poison | Spikes | Magic
    deriving Show

magicPhrase :: Phrase -> Room -> Either (Room, Trap) Door
magicPhrase _           Entrance       = Right (Door SpookyCorridor)
magicPhrase Alakazam    SpookyCorridor = Right (TrappedDoor Explosion TreasureRoom)
magicPhrase _           SpookyCorridor = Left (SpookyCorridor, Magic)
magicPhrase _           TreasureRoom   = Right (TrappedDoor Poison ThroneRoom)
magicPhrase OpenSesame  ThroneRoom     = Right (Door Dungeon)
magicPhrase _           ThroneRoom     = Right (TrappedDoor Spikes Dungeon)
magicPhrase Abracadabra Dungeon        = Right (Door Entrance)
magicPhrase _           Dungeon        = Left (Dungeon, Magic)

openDoor :: Door -> Either (Room, Trap) Room
openDoor (Door r) = Right r
openDoor (TrappedDoor t r) = Left (r, t)

disarmExplosion :: Door -> Either (Room, Trap) Door
disarmExplosion d@(Door r) = Right d
disarmExplosion (TrappedDoor Explosion r) = Right (Door r)
disarmExplosion (TrappedDoor t r) = Left (r, t)

disarmPoison :: Door -> Either (Room, Trap) Door
disarmPoison d@(Door r) = Right d
disarmPoison (TrappedDoor Poison r) = Right (Door r)
disarmPoison (TrappedDoor t r) = Left (r, t)

disarmSpikes :: Door -> Either (Room, Trap) Door
disarmSpikes d@(Door r) = Right d
disarmSpikes (TrappedDoor Spikes r) = Right (Door r)
disarmSpikes (TrappedDoor t r) = Left (r, t)

one = magicPhrase Alakazam Entrance >>= openDoor >>= magicPhrase Alakazam >>= disarmExplosion >>= openDoor >>= magicPhrase OpenSesame >>= openDoor
two = magicPhrase OpenSesame Entrance >>= openDoor >>= magicPhrase OpenSesame >>= openDoor
three = magicPhrase OpenSesame ThroneRoom >>= openDoor >>= magicPhrase Abracadabra >>= openDoor
finish = magicPhrase Alakazam Entrance >>= openDoor >>= magicPhrase Alakazam >>= disarmExplosion >>= openDoor >>= magicPhrase Alakazam >>= disarmPoison >>= openDoor >>= magicPhrase Abracadabra >>= disarmSpikes >>= openDoor >>= magicPhrase Abracadabra >>= openDoor
