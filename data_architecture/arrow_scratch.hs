import Control.Arrow

b2e :: (a -> Bool) -> a -> Either a a
b2e f x
    | f x = Right x
    | otherwise = Left x

branch :: ArrowChoice a =>
    (b -> Bool) -> a b c -> a b d -> a b (Either c d)
branch c l r = l +++ r <<< arr (b2e c)

