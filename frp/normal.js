$(document).ready(function(){
    var draw = SVG('drawing')

    var offX = 300;
    var offY = 200;
    var radius = 100;
    var minR = radius*0.5;
    var maxR = radius;

    var circle = draw.circle(radius*2).center(offX,offY).fill('#000');
    var circle2 = draw.circle(minR*2).center(offX, offY).fill("#fff");
    var tick = draw.line(offX, offY-minR, offX, offY-maxR).stroke({width: 1, color: '#fff'});
    var volume = draw.text("0").center(offX, offY).font({size: 14, color: "#f00"});

    var power = false;
    var volumeN = 0;
    var oldCoords = [0, 0];

    circle.mousedown(function(){
        power = true;
    });
    $(document).mouseup(function(){
        power = false;
    });
    circle.mousemove(function(e){
        var cartesian = normalize([e.x, e.y], offX, offY);
        var polar = toPolar(cartesian);
        if( power && polar[0] >= minR && polar[0] <= maxR ){
            tickCoords = [
                minR*Math.cos(polar[1]) + offX,
                minR*Math.sin(polar[1]) + offY,
                maxR*Math.cos(polar[1]) + offX,
                maxR*Math.sin(polar[1]) + offY
            ];
            tick.plot(tickCoords[0], tickCoords[1],
                      tickCoords[2], tickCoords[3]);

            if(oldCoords[1] <= polar[1]){
                volumeN++;
            }
            else{
                if(volumeN > 0){
                    volumeN--;
                }
            }
            volume.text(volumeN + "");
        }
        oldCoords = polar;
    });

    function normalize(coords, x, y){
        return [coords[0] - offX, coords[1] - offY];
    }

    function toPolar(coords){
        return [
            Math.sqrt(coords[0]*coords[0] + coords[1]*coords[1]),
            Math.atan2(coords[1], coords[0])
            ];
    }
});
