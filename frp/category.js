// Generated by CoffeeScript 1.7.1
(function() {
  Bacon.Observable.prototype.fmap = function(f) {
    return this.map(function(v) {
      return v.fmap(f);
    });
  };

  Bacon.Observable.prototype.pass = function(f) {
    return this.map(function(v) {
      return v.pass(f);
    });
  };

}).call(this);
