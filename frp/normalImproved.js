$(document).ready(function(){
    var draw = SVG('drawing')

    var offX = 300;
    var offY = 200;
    var radius = 100;
    var minR = radius*0.5;
    var maxR = radius;

    var circle = draw.circle(radius*2).center(offX,offY).fill('#000');
    var circle2 = draw.circle(minR*2).center(offX, offY).fill("#fff");
    var tick = draw.line(offX, offY-minR, offX, offY-maxR).stroke({width: 1, color: '#fff'});
    var volume = draw.text("0").center(offX, offY).font({size: 14, color: "#f00"});

    var positionBus = new Bacon.Bus();
    var powerBus = new Bacon.Bus();
    var powerProp = powerBus.toProperty();

    circle.mousedown(function(){
        powerBus.push(Maybe.pure(undefined));
    });
    $(document).mouseup(function(){
        powerBus.push(Maybe.Nothing);
    });
    circle.mousemove(function(e){
        positionBus.push(Maybe.pure([e.x, e.y]));
    });

    var filtered = positionBus.fmap(function(coords){
        return normalize(coords, offX, offY);
    }).
    fmap(function(cartesian){
        return toPolar(cartesian);
    }).
    pass(function(polar){
        if(polar[0] >= minR && polar[0] <= maxR){
            return Maybe.pure(polar);
        }
        else {
            return Maybe.Nothing;
        }
    });

    var withPower = Bacon.combineWith(function(power, position){
        if(power.isJust()){
            return position;
        }
        else {
            return Maybe.Nothing;
        }
    }, powerProp, filtered);

    //plot new tick
    withPower.fmap(function(polar){
        return [
            minR*Math.cos(polar[1]) + offX,
            minR*Math.sin(polar[1]) + offY,
            maxR*Math.cos(polar[1]) + offX,
            maxR*Math.sin(polar[1]) + offY
        ];
    }).onValue(function(coords){
        if(coords.isJust){
            coords = coords.val;
            tick.plot(coords[0], coords[1],
                      coords[2], coords[3]);
        }
    });

    var justTheAngle = withPower.throttle(100).fmap(function(polar){
        return polar[1];
    });
    var offsetByOne = justTheAngle.startWith(Maybe.pure(0));
    Bacon.zipWith([offsetByOne, justTheAngle], function(prev, current){
        if(prev.isJust() && current.isJust()){
            return Maybe.pure(prev.val < current.val);
        }
        return Maybe.Nothing;
    }).fmap(function(direction){
        return direction ? 1 : -1;
    }).fmap(function(value){
        return new Plus(value);
    }).scan(Maybe.pure(Plus.mempty()), function(accum, value){
        var combined = accum.combine(value);
        if(combined.val.val > 0){
            return combined;
        }
        else {
            return Maybe.pure(Plus.mempty());
        }
    }).fmap(function(v){
        return v.val;
    }).onValue(function(v){
        if(v.isJust()){
            v = v.val;
            volume.text(v + "");
        }
    });

    function normalize(coords, x, y){
        return [coords[0] - offX, coords[1] - offY];
    }

    function toPolar(coords){
        return [
            Math.sqrt(coords[0]*coords[0] + coords[1]*coords[1]),
            Math.atan2(coords[1], coords[0])
            ];
    }
});
