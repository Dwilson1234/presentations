Bacon.Observable::fmap = (f) ->
    @map((v) -> v.fmap(f))

Bacon.Observable::pass = (f) ->
    @map((v) -> v.pass(f))
