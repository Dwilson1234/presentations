/*
 * TODO: map -> fmap
 */
$(document).ready(function(){
    var draw = SVG('drawing')

    var offX = 300;
    var offY = 200;
    var radius = 100;
    var minR = radius*0.5;
    var maxR = radius;

    var circle = draw.circle(radius*2).center(offX,offY).fill('#000');
    var circle2 = draw.circle(minR*2).center(offX, offY).fill("#fff");
    var tick = draw.line(offX, offY-minR, offX, offY-maxR).stroke({width: 1, color: '#fff'});
    var volume = draw.text("0").center(offX, offY).font({size: 14, color: "#f00"});

    var position = new Bacon.Bus();

    circle.mousemove(function(e){
        // TODO: using maybe
        position.push([e.x, e.y]);
    });

    var filtered = position.
        map(function(v){
            return [v[0]-offX, v[1]-offY];
        }).
        map(function(v){
            return [
                Math.sqrt(v[0]*v[0] + v[1]*v[1]),
                Math.atan2(v[1], v[0])
                ];
        }).
        // TODO: use pass
        filter(function(v){
            return v[0] >= minR && v[0] <= maxR;
        });

    var toPlot = filtered.
        map(function(v){
            return [
                minR*Math.cos(v[1]),
                minR*Math.sin(v[1]),
                maxR*Math.cos(v[1]),
                maxR*Math.sin(v[1])
                ];
        }).
        map(function(v){
            return [v[0]+offX, v[1]+offY, v[2]+offX, v[3]+offY];
        });

    toPlot.onValue(function(v){
        tick.plot(v[0], v[1], v[2], v[3]);
    });

    var justTheAngle = filtered.
        throttle(100).
        map(function(v){
            return v[1];
        });
    // TODO: Use Maybes
    var filteredOffByOne = justTheAngle.startWith(0);
    var volumeStream = Bacon.
        // TODO: use maybes
        zipWith([filteredOffByOne, justTheAngle], function(v1, v2){
            return v1 < v2;
        }).
        map(function(v){
            return v ? 1 : -1;
        }).
        // TODO Use maybes, then monoids
        scan(0, function(accum, v){
            if( accum + v > 0 ){
                return accum + v;
            }
            else {
                return 0;
            }
        });

    volumeStream.onValue(function(v){
        volume.text(v + "");
    });
});
