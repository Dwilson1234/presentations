$(document).ready(function(){
    var draw = SVG('drawing')

    var offX = 300;
    var offY = 200;
    var radius = 100;
    var minR = radius*0.5;
    var maxR = radius;

    var circle = draw.circle(radius*2).center(offX,offY).fill('#000');
    var circle2 = draw.circle(minR*2).center(offX, offY).fill("#fff");
    var tick = draw.line(offX, offY-minR, offX, offY-maxR).stroke({width: 1, color: '#fff'});
    var volume = draw.text("0").center(offX, offY).font({size: 14, color: "#f00"});

    var position = new Bacon.Bus();
    var powerBus = new Bacon.Bus();
    power = powerBus.toProperty();

    circle.mousedown(function(){
        powerBus.push(Maybe.pure(undefined));
    });
    $(document).mouseup(function(){
        powerBus.push(Maybe.Nothing);
    });
    circle.mousemove(function(e){
        position.push([e.x, e.y]);
    });

    var onDial = position.
        map(function(v){
            return [v[0]-offX, v[1]-offY];
        }).
        map(function(v){
            return [
                Math.sqrt(v[0]*v[0] + v[1]*v[1]),
                Math.atan2(v[1], v[0])
                ];
        }).
        filter(function(v){
            return v[0] >= minR && v[0] <= maxR;
        });

    var filtered = Bacon.combineWith(function(power, position){
        if(power.isNothing()){
            return power;
        }
        else{
            return Maybe.pure(position);
        }
    }, power, onDial);

    var toPlot = filtered.
        fmap(function(v){
            return [
                minR*Math.cos(v[1]),
                minR*Math.sin(v[1]),
                maxR*Math.cos(v[1]),
                maxR*Math.sin(v[1])
                ];
        }).
        fmap(function(v){
            return [v[0]+offX, v[1]+offY, v[2]+offX, v[3]+offY];
        });

    toPlot.onValue(function(v){
        if(v.isJust()){
            var w = v.val
            tick.plot(w[0], w[1], w[2], w[3]);
        }
    });

    var justTheAngle = filtered.
        throttle(100).
        fmap(function(v){
            return v[1];
        });
    var filteredOffByOne = justTheAngle.startWith(Maybe.pure(0));
    var volumeStream = Bacon.
        // Start by zipping the angle stream and the offset angle stream
        // together with a comparison function. Diagram:
        // angle               : 0 1 2 1 4 ? ...
        // angleOffset         : 0 0 1 2 1 4 ...
        // =====================================
        // angle < angleOffset : f f f t f ? ...
        zipWith([filteredOffByOne, justTheAngle], function(v1, v2){
            if(v1.isJust() && v2.isJust()){
                return Maybe.pure(v1.val < v2.val);
            }
            return Maybe.Nothing;
        }).
        fmap(function(v){
            return v ? 1 : -1;
        }).
        fmap(function(v){
            return new Plus(v);
        }).
        scan(Maybe.pure(Plus.mempty()), function(accum, v){
            var combined = accum.combine(v);
            if(combined.isJust() && 
               combined.val.val > 0){
                return combined;
            }
            else {
                return Maybe.pure(Plus.mempty());
            }
        }).
        fmap(function(v){
            return v.val;
        });

    volumeStream.onValue(function(v){
        if(v.isJust()){
            var w = v.val;
            volume.text(w + "");
        }
    });
});
