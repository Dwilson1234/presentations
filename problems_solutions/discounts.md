We have an order and a possible set of discounts.

A discount is a function that takes an order and returns a new order that is
identical to the old order but with the discount applied.

Each discount function can be considered a member of the Set of Discount
Functions (S~D~).

Set of (Order, Discount) pairs?

We need a function that, for a given Order, finds the maximum possible discount.
If we treat our set as (Order, Discount) pairs, then all we must do is calculate
each discount and find the maximum. Our function might have the signature

`f :: [(Order, Discount)] -> (Order, Discount)`

Practically we'd be operating on a subset of the whole set where each Order was
the same. We have a number of ways we can implement this, but they all come down
to the same binary operation: Given two (Order, Discount) pairs, find the one
that results in a greater discount.

`greater_discount :: (Order, Discount) -> (Order, Discount) -> (Order, Discount)`

The only restriction on the binary operation is that it must be closed, that is,
it must result in another member of the same set. This pattern consisting of a
set of objects with a binary operation appears frequently once you start
recognizing it as a pattern. Almost any binary arithmetic operation falls under
this pattern. Many uses of reduce also fall under this pattern. It occurs so
frequently that it has been given a name: Magma. It's also sometimes called a
groupoid.

Naming things has value. That is why the Gang of Four book was so popular. It
named and formalized a lot of patterns that we may have been doing on our own.
Once somethin is named it allows discussion around it, as well as further
investigation. For example, we can start adding properties to the binary
operation in the magma. If we start by saying that the operation must be
associative, well now we have something called a semigroup. Associativity is
nice because it means we don't care about grouping. If we then also say that
there must be an identity element with respect to the binary operation, well now
we've defined a monoid. As an example, the natural numbers form a monoid under
addition, where the identity element is 0. They also form a different monoid
under multiplication where the identity element is 1.

What does all this talk about magmas, semigroups, and monoids get us? Well at
the very least we can abstract away certain patterns. With the example of
reductions: We can define a single method to perform a reduction over **all**
monoids. If we further stipulate that the reduced set be non-empty, we can relax
the restriction to **all** semigroups. What does this mean for code structure?
It means that we can keep our behavior close to our data specification. <fill in>

TODO: Returning back to our discount example.

We also have a set of functions that take an order and a set of discounts. These
functions apply the maximum discount to the order, returning the discounted
order.
