module Types where

import Nats
import Prelude
import Data.Either
import Data.List
import Control.Arrow
import Data.Monoid
import Data.Maybe
import Safe
import Control.Applicative
import Database.Persist.Class
import Database.Persist.Sql
import qualified Data.Text as T

boardSize = (boardWidth, boardHeight) :: (Natural, Natural)
boardWidth = 8 :: Natural
boardHeight = 8 :: Natural

data Piece = Pawn | Bishop | Rook | Knight | King | Queen
  deriving (Show)
data ChessTeam = Black | White
  deriving (Eq, Show)

otherTeam Black = White
otherTeam White = Black

data ChessPiece = ChessPiece
  { chessPiecePiece :: Piece
  , chessPieceSquare :: ChessSquare
  , chessPieceTeam :: ChessTeam
  }
  deriving (Show)
data ChessSquare = ChessSquare
  { chessSquareCol :: Natural
  , chessSquareRow :: Natural
  }
  deriving (Eq, Show)
type Board = [ChessPiece]

mkChessPiece :: (Piece, (Natural, Natural), ChessTeam) -> ChessPiece
mkChessPiece (p, (x, y), t) = ChessPiece p (ChessSquare x y) t

initialBoard :: Board
initialBoard = map mkChessPiece $ white <> black
    where white = map (\(p, xy) -> (p, xy, White)) $ mkRow 0 powerRow <> mkRow 1 pawnRow
          black = map (\(p, xy) -> (p, xy, Black)) $ mkRow (boardWidth-1) powerRow <> mkRow (boardWidth-2) pawnRow
          mkRow y row = map (\(p, x) -> (p, (x,y))) row
          powerRow = [(Rook, 0), (Knight, 1), (Bishop, 2), (Queen, 3), (King, 4), (Bishop , 5), (Knight, 6), (Rook, 7)]
          pawnRow = [(Pawn, x) | x <- [0..boardWidth-1]]

forward :: ChessSquare -> ChessSquare
forward (ChessSquare x y) = ChessSquare x y'
    where y' = if y+1 < boardWidth then y+1 else y

backward :: ChessSquare -> ChessSquare
backward (ChessSquare x y) = ChessSquare x (y-1)

leftward :: ChessSquare -> ChessSquare
leftward (ChessSquare x y) = ChessSquare (x-1) y

rightward :: ChessSquare -> ChessSquare
rightward (ChessSquare x y) = ChessSquare x' y
    where x' = if x+1 < boardWidth then x+1 else x

pieceAt :: Board -> ChessSquare -> Maybe ChessPiece
pieceAt board pos = find ((pos ==) . chessPieceSquare) board

squareIfPieceIsOnIt :: Board -> ChessTeam -> ChessSquare -> Maybe ChessSquare
squareIfPieceIsOnIt board team pos = fmap chessPieceSquare $ find pred board
    where pred = uncurry (&&) . ((pos ==) . chessPieceSquare &&& (team ==) . chessPieceTeam)

squareIfPieceIsNotOnIt :: Board -> ChessTeam -> ChessSquare -> Maybe ChessSquare
squareIfPieceIsNotOnIt board team pos =
    case (find pred board) of
        Just _ -> Nothing
        Nothing -> Just pos
    where pred = uncurry (&&) . ((pos ==) . chessPieceSquare &&& (team ==) . chessPieceTeam)

colRow :: ChessSquare -> (Natural, Natural)
colRow = chessSquareCol &&& chessSquareRow

colsRows :: ChessSquare -> ChessSquare -> ((Natural, Natural), (Natural, Natural))
colsRows = curry $ colRow *** colRow

diagonalNaturals :: (Natural, Natural) -> (Natural, Natural) -> Bool
diagonalNaturals (x, y) (x', y') = (x `diffNat` x') == (y `diffNat` y')

isDiagonal :: ChessSquare -> ChessSquare -> Bool

isDiagonal = curry $ uncurry diagonalNaturals . uncurry colsRows

isKnight :: ChessSquare -> ChessSquare -> Bool
isKnight (ChessSquare x1 y1) (ChessSquare x2 y2) = (x1 `diffNat` x2) + (y1 `diffNat` y2) == 3

skipSquare :: ChessSquare -> [ChessSquare] -> [ChessSquare]
skipSquare sq = filter (sq /=)

--TODO clean this up, there's lots of duplication
moves :: Board -> ChessPiece -> [ChessSquare]
moves board (ChessPiece Pawn pos team) = catMaybes $ capturing <> potential
    where capturing = map (squareIfPieceIsOnIt board (otherTeam team)) . map ($ pos) . map (forward . ) $ [leftward, rightward]
          potential = [squareIfPieceIsNotOnIt board team (forward pos)]

moves board (ChessPiece Bishop pos team) = nub . skipSquare pos $ potential <> captures
    where potential =  concatMap (takeTilTheirTeam . takeTilMyTeam) possibleMoves
          captures = catMaybes . map (headMay . catMaybes . map (squareIfPieceIsOnIt board (otherTeam team)) . takeTilMyTeam) $ possibleMoves
          possibleMoves = map (takeDiagonals . flip iterate pos) [forward.leftward, forward.rightward, backward.leftward, backward.rightward]
          takeDiagonals = takeWhile (isDiagonal pos)
          takeTilMyTeam = takeWhile (isJust . squareIfPieceIsNotOnIt board team)
          takeTilTheirTeam = takeWhile (isJust . squareIfPieceIsNotOnIt board (otherTeam team))

moves board (ChessPiece Rook pos team) = nub . skipSquare pos $ potential <> capturing
    where potential =  nub . concatMap (takeTilTheirTeam . takeTilMyTeam) $ possibleMoves
          capturing = catMaybes . map (headMay . catMaybes . map (squareIfPieceIsOnIt board (otherTeam team)) . takeTilMyTeam) $ possibleMoves
          possibleMoves = map (takeOnBoard . flip iterate pos) [forward, backward, leftward, rightward]
          takeOnBoard = take (fromIntegral $ toInteger boardWidth)
          takeTilMyTeam = takeWhile (isJust . squareIfPieceIsNotOnIt board team)
          takeTilTheirTeam = takeWhile (isJust . squareIfPieceIsNotOnIt board (otherTeam team))

moves board (ChessPiece Knight pos team) = potential
    where potential = filter (\p -> isKnight pos p && (hasTheirs p || not (hasOurs p))) possibleMoves
          hasTheirs = isJust . squareIfPieceIsOnIt board (otherTeam team)
          hasOurs = isJust . squareIfPieceIsOnIt board team
          possibleMoves = map ($ pos) [ forward.forward.leftward
                                      , forward.forward.rightward
                                      , backward.backward.leftward
                                      , backward.backward.rightward
                                      , leftward.leftward.forward
                                      , leftward.leftward.backward
                                      , rightward.rightward.forward
                                      , rightward.rightward.backward ]

moves board (ChessPiece Queen pos team) = nub . skipSquare pos $ potential <> capturing
    where potential =  concatMap (takeTilTheirTeam . takeTilMyTeam) possibleMoves
          capturing = catMaybes . map (headMay . catMaybes . map (squareIfPieceIsOnIt board (otherTeam team)) . takeTilMyTeam) $ possibleMoves
          possibleMoves = possibleDiagMoves <> possibleStraightMoves
          possibleDiagMoves = map (takeDiagonals . flip iterate pos) [forward.leftward, forward.rightward, backward.leftward, backward.rightward]
          possibleStraightMoves = map (takeOnBoard . flip iterate pos) [forward, backward, leftward, rightward]
          takeDiagonals = takeWhile (isDiagonal pos)
          takeOnBoard = take (fromIntegral $ toInteger boardWidth)
          takeTilMyTeam = takeWhile (isJust . squareIfPieceIsNotOnIt board team)
          takeTilTheirTeam = takeWhile (isJust . squareIfPieceIsNotOnIt board (otherTeam team))

moves board (ChessPiece King pos team) = nub . skipSquare pos $ potential
    where potential = filter (\p -> (hasTheirs p || not (hasOurs p))) possibleMoves
          hasTheirs = isJust . squareIfPieceIsOnIt board (otherTeam team)
          hasOurs = isJust . squareIfPieceIsOnIt board team
          possibleMoves = map ($ pos) [ forward
                                      , backward
                                      , rightward
                                      , leftward
                                      , forward.leftward
                                      , forward.rightward
                                      , backward.leftward
                                      , backward.rightward ]

piecesForTeam :: ChessTeam -> Board -> [ChessPiece]
piecesForTeam team = filter ((team==) . chessPieceTeam)

isThreatened :: ChessTeam -> Board -> ChessSquare -> Bool
isThreatened team board sq = isJust . find (sq==) . concatMap (moves board) $ (piecesForTeam team board)

getPieceAt :: Board -> NaturalPair -> Maybe ChessPiece
getPieceAt board np = find ((np ==) . NaturalPair . colRow . chessPieceSquare) board

movePiece :: Board -> (ChessSquare, ChessSquare) -> Board
movePiece board (from, to) = map moveIt board
    where moveIt p@(ChessPiece _ sq _) = if sq==from then p{chessPieceSquare=to} else p

instance PersistField Natural where
  toPersistValue = PersistInt64 . fromIntegral . natToInt
  fromPersistValue (PersistInt64 int) = Right . fromInteger . fromIntegral $ int

instance PersistFieldSql Natural where
  sqlType _ = SqlInt64

newtype NaturalPair = NaturalPair { runNaturalPair :: (Natural, Natural) }
  deriving (Show, Read, Eq)

instance PersistField NaturalPair where
  toPersistValue = PersistText . T.pack . show
  fromPersistValue (PersistText pair) = Right . read . T.unpack $ pair

instance PersistFieldSql NaturalPair where
  sqlType _ = SqlString
