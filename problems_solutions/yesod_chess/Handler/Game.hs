{-# LANGUAGE OverloadedStrings #-}
module Handler.Game where

import Import
import Types

getGameR :: GameId -> Handler Html
getGameR gameId = do
    (game, user1, user2, moves) <- runDB $ do
        game@(Game user1_id user2_id _) <- get404 gameId
        moves <- selectList [MoveGameId ==. gameId] [Asc MoveCreated]
        user1 <- getJust user1_id
        user2 <- case user2_id of
            Just user2_id -> get user2_id
            Nothing -> return Nothing
        return (game, user1, user2, moves)
    let moves' = map (\m -> (uncurry ChessSquare . runNaturalPair . moveStart_position $ m, uncurry ChessSquare . runNaturalPair . moveEnd_position $ m)) . map entityVal $ moves
    let board = foldl movePiece initialBoard moves'
    defaultLayout $ do
        setTitle $ toHtml $ userIdent user1 <> " vs. " <> maybe "Nobody" userIdent user2
        $(widgetFile "game")

chessSquare row col board gameId = $(widgetFile "chessSquare")
    where color = if (row + col) `mod` 2 == 0 then "white" else "black" :: Text
          content board = case getPieceAt board (NaturalPair (fromInteger (col-1), boardHeight - fromInteger row)) of
            Just (ChessPiece Pawn _ Black) -> preEscapedToMarkup ("&#9823;" :: Text)
            Just (ChessPiece Knight _ Black) -> preEscapedToMarkup ("&#9822;" :: Text)
            Just (ChessPiece Bishop _ Black) -> preEscapedToMarkup ("&#9821;" :: Text)
            Just (ChessPiece Rook _ Black) -> preEscapedToMarkup ("&#9820;" :: Text)
            Just (ChessPiece Queen _ Black) -> preEscapedToMarkup ("&#9819;" :: Text)
            Just (ChessPiece King _ Black) -> preEscapedToMarkup ("&#9818;" :: Text)
            Just (ChessPiece Pawn _ White) -> preEscapedToMarkup ("&#9817;" :: Text)
            Just (ChessPiece Knight _ White) -> preEscapedToMarkup ("&#9816;" :: Text)
            Just (ChessPiece Bishop _ White) -> preEscapedToMarkup ("&#9815;" :: Text)
            Just (ChessPiece Rook _ White) -> preEscapedToMarkup ("&#9814;" :: Text)
            Just (ChessPiece Queen _ White) -> preEscapedToMarkup ("&#9813;" :: Text)
            Just (ChessPiece King _ White) -> preEscapedToMarkup ("&#9812;" :: Text)
            Nothing -> ""
chessRow row board gameId = $(widgetFile "chessRow")
chessBoard board gameId = $(widgetFile "chessBoard")

-- TODO fix this
rowIndices = [1..(toInteger boardHeight)]
columnIndices = [1..(toInteger boardWidth)]
